# Additional Info

## General Notes
Both tasks can be seen on the following links 🤖

 - Test 1 - [https://jsfiddle.net/BrumGB/73o48txh/](https://jsfiddle.net/BrumGB/73o48txh/)
 - Test 2 - [https://jsfiddle.net/BrumGB/hx60tuzs/](https://jsfiddle.net/BrumGB/hx60tuzs/)

## Test 1
This approach will require the code to be transpiled as es6 features will not work on older browsers. (This is usually added in the build process)
